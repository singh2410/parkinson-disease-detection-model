#!/usr/bin/env python
# coding: utf-8

# # Parkinson’s Disease Detection
# #By- Aarush Kumar
# #Dated: June 19,2021

# In[1]:


get_ipython().system('pip install numpy pandas sklearn xgboost')


# In[2]:


import pandas as pd
import numpy as np
import os, sys
from sklearn.preprocessing import MinMaxScaler
from xgboost import XGBClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score


# In[3]:


df=pd.read_csv(r'/home/aarush100616/Downloads/Projects/Parkinson Disease Detection/parkinsons.data')


# In[4]:


df


# In[5]:


df.head()


# ## EDA

# In[7]:


df.shape


# In[8]:


df.size


# In[9]:


df.info()


# In[11]:


df.isnull().sum()


# ## LabelEncoding

# In[12]:


features=df.loc[:,df.columns!='status'].values[:,1:]
labels=df.loc[:,'status'].values


# In[13]:


#Get the count of each label (0 and 1) in labels
print(labels[labels==1].shape[0], labels[labels==0].shape[0])


# In[14]:


#MinMaxScaler- Scale the features to between -1 and 1
scaler=MinMaxScaler((-1,1))
x=scaler.fit_transform(features)
y=labels


# ## Splitting Data

# In[15]:


x_train,x_test,y_train,y_test=train_test_split(x, y, test_size=0.2, random_state=7)


# In[16]:


x_train


# In[17]:


x_test


# In[18]:


y_train


# In[19]:


y_test


# ## XGB Classifier

# In[20]:


#Train the model
model=XGBClassifier()
model.fit(x_train,y_train)


# ## Finding the accuracy of model

# In[22]:


#Calculate the accuracy
y_pred=model.predict(x_test)
print(accuracy_score(y_test, y_pred)*100)

